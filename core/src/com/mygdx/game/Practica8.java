package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;

public class Practica8 extends ApplicationAdapter implements InputProcessor {
	SpriteBatch batch;
	//Texture img;
	TiledMap tiledMap;
	OrthographicCamera cam;
	TiledMapRenderer tiledMapRenderer;
	ParticleEffect part;
	
	@Override
	public void create () {
		batch = new SpriteBatch();
		//img = new Texture("badlogic.jpg");
		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();
		cam = new OrthographicCamera();
		cam.setToOrtho(false,w,h);
		cam.update();
		tiledMap = new TmxMapLoader().load("Test.tmx");
		tiledMapRenderer = new OrthogonalTiledMapRenderer(tiledMap);
		part = new ParticleEffect();
		part.load(Gdx.files.internal("BlueFire"),Gdx.files.internal(""));
		part.setPosition(Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()/2);
		part.start();
		Gdx.input.setInputProcessor(this);

	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		//part.update(Gdx.graphics.getDeltaTime());
		batch.begin();
		//part.draw(batch);
		//batch.draw(img, 0, 0);
		cam.update();
		tiledMapRenderer.setView(cam);
		tiledMapRenderer.render();
		//part.draw(batch);
		batch.end();
		part.update(Gdx.graphics.getDeltaTime());
		batch.begin();
		part.draw(batch);
		batch.end();
		if (part.isComplete()) part.reset();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
		//img.dispose();
	}

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		if(keycode == Input.Keys.LEFT)
			cam.translate(-32,0);
		if(keycode == Input.Keys.RIGHT)
			cam.translate(32,0);
		if(keycode == Input.Keys.UP)
			cam.translate(0,32);
		if(keycode == Input.Keys.DOWN)
			cam.translate(0,-32);
		if(keycode == Input.Keys.NUM_1) {
			tiledMap.getLayers().get(0).setVisible(
					!tiledMap.getLayers().get(0).isVisible());
			part.scaleEffect(0.5f);
		}
		if(keycode == Input.Keys.NUM_2)
			tiledMap.getLayers().get(1).setVisible(
					!tiledMap.getLayers().get(1).isVisible());
		if(keycode == Input.Keys.NUM_4) {
			part.scaleEffect(1.5f);
		}
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {

		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		part.setPosition(screenX, Gdx.graphics.getHeight() - screenY);
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
}
